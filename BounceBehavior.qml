import QtQuick 2.15

Behavior {
  id: root
  property Item target: targetProperty.object

  ParallelAnimation {
    SequentialAnimation {
      NumberAnimation {
        target: root.target
        property: "x"
        to: root.target.nextX
        easing.type: Easing.OutQuad
      }
      NumberAnimation {
        target: root.target
        property: "x"
        to: 0
        easing.type: Easing.InQuad
      }
    }

    SpringAnimation {
      target: root.target
      property: "y"
      spring: 2; damping: 0.5; mass: 1.0 
      to: targetValue
    }

    // NumberAnimation { 
    //   target: root.target
    //   property: "y"
    //   to: targetValue
    //   easing.type: Easing.InOutCubic
    // }
  }
}
