Note: you need PySide6 installed to run this
Follow the instructions at https://pypi.org/project/PySide6/ to add it to your python environment

Then run `python3 main.py` to execute the client.
Then run the ImprobableRaceServer.
The Client will immediately start receiving updates and displaying them on the screen
(The server can be started before the client as well.)

The yellow runner is the one with the fastest estimated completion time
The green runners are estimated to complete the race in less then 13 secs
The red runners are estimated to complete the race in more the 40 secs
The blue runners are everyone else.