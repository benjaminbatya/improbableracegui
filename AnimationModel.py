import logging
import time
from functools import cmp_to_key

from Runner import Runner
import CourseData

from PySide6.QtCore import QObject, QTimer, Signal, Slot
from PySide6.QtCore import Qt

logger = logging.getLogger("AnimationModel")

UPDATE_PERIOD_MS = 0.1
ANIM_PERIOD_MS = 3

ID_ROLE   = Qt.UserRole + 0
DIST_ROLE = Qt.UserRole + 1
TIME_ROLE = Qt.UserRole + 2
VELO_ROLE = Qt.UserRole + 3
POS_ROLE  = Qt.UserRole + 4

class AnimationModel(QObject):
  timeUpdated = Signal(float, bool) # Signal params are (currTime, doUpdate)
  runnerAdded = Signal(int) # indicates that a new runner has been added at index 

  def __init__(self, parent=None):
    super().__init__(parent)

    self._runners = [] # Mapping of index to runner #

    self._lastTime = time.time()
    self._timeDelta = 0

    # Start firing updates every ANIM_PERIOD_MS to get very smooth animation
    self._timer = QTimer(self)
    self._timer.setInterval(ANIM_PERIOD_MS)
    self._timer.setSingleShot(False)
    self._timer.timeout.connect(self._onTimeout)

    self._updates = 0
    self._numFinishes = 0
  
  def start(self):
    self._timer.start(ANIM_PERIOD_MS)

  def stop(self):
    self._timer.stop()

  @Slot()
  def _onTimeout(self):
    now = time.time()
    if(now-self._lastTime < UPDATE_PERIOD_MS): return

    self._lastTime = now

    # Calculate the race time using the time delta
    race_time = 0
    if self._timeDelta != 0.0:
      race_time = now-self._timeDelta

    # logger.info("_onTimeout: race_time=%f", race_time)

    for r in self._runners:
      r.updatePosition(race_time)

    self._updates += 1
    doUpdate = self._updates%10 == 0
    if doUpdate:
      self._runners.sort(key=cmp_to_key(lambda x,y: x.estimatedTime-y.estimatedTime))

    self.timeUpdated.emit(race_time, doUpdate)

  @Slot(float, int, int)
  def onProgress(self, currTime, pId, gate):
    # logger.info("onProgress: %f, %d, %d", currTime, pId, gate)

    # NOTE: currTime is the correct time. timeDelta is used to adjust the models clock
    # update the timeDelta
    now = time.time()
    self._timeDelta = now - currTime

    runner = next((x for x in self._runners if x.pId==pId), None)

    if runner:
      # logger.info("runner %d passed gate %d", pId, gate)
      runner.passGate(gate, currTime)

      if gate==self.getNumGates():
        self._numFinishes += 1
        # Stop the timer when all of the runners are finished
        if self._numFinishes==len(self._runners):
          self.stop()
          self.timeUpdated.emit(currTime, True)
      
    else:
      # logger.info("runner %d added", pId)
      runner = Runner(len(self._runners), pId, gate, currTime)
      self._runners.append(runner)
      self.runnerAdded.emit(runner.idx)

  @Slot(result=int)
  def getNumGates(self):
    return CourseData.NUM_GATES

  @Slot(result=int)
  def getNumRunners(self):
    return len(self._runners)

  @Slot(int, result=int)
  def getIdx(self, idx):
    return self._runners[idx].idx

  @Slot(int, result=int)
  def getPId(self, idx):
    return self._runners[idx].pId

  @Slot(int, result=float)
  def getPosition(self, idx):
    return self._runners[idx].position

  @Slot(int, result=float)
  def getVelocity(self, idx):
    return self._runners[idx].velocity

  @Slot(int, result=float)
  def getEstimatedTime(self, idx):
    return self._runners[idx].estimatedTime

  @Slot(int, result=float)
  def getCourseRecord(self, idx):
    # if idx<1 or idx>len(CourseData.FASTEST_RUNNER_TIMES):
    time = CourseData.FASTEST_RUNNER_TIMES[idx-1]
    # logger.info("idx=%d, time=%f", idx, time)
    return time

  @Slot(int, result=bool)
  def isOnRecordPace(self, idx):
    # currtime = self._lastTime-self._timeDelta
    # return currtime<CourseData.FASTEST_RUNNER_TIMES[int(pos)]
    time = self.getEstimatedTime(idx)
    return time<CourseData.FASTEST_RUNNER_TIMES[-1]

  @Slot(int, result=bool)
  def isSlow(self, idx):
    time = self.getEstimatedTime(idx)
    return time>32
