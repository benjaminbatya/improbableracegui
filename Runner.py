import logging

import CourseData

GATE = 0
TIME = 1

NOT_COMPLETED_TIME = 1000000

logger = logging.getLogger("Runner")
class Runner:

  def __init__(self, idx, pId, gate, time):
    self._idx = idx
    self._pId = pId                           # Runner ID
    self.position = 0                         # Estimated position of runner
    self.velocity = 0                         # Current estimated velocity
    self._events = []                         # list of events for this runner
    self.estimatedTime = NOT_COMPLETED_TIME   # Indicates the current estimated course record
    self.courseRecord = False                 # indicates if current pace is a new course record

    self.passGate(gate, time)

  @property
  def pId(self):
    return self._pId

  @property
  def idx(self):
    return self._idx

  def passGate(self, gate, time):
    self._events.append((gate, time))

    self.position = gate

    if gate == CourseData.NUM_GATES:
      self.estimatedTime = time
      self.velocity = self.position / time

      if(time < CourseData.FASTEST_RUNNER_TIMES[-1]):
        logger.info("Runner %d set a new course record at %fs", self.pId, time)
        
    else:
      # if len(self._events) > 1:
      #   # Calculate global velocity. Because the velocity can change unpredictably, 
      #   # the global velocity (vs the per-gate velocity) seems to give a better estimatedTime.
      #   # NOTE: this assumes that the gates are one unit apart and equally spaced out
      #   self.velocity = gate / time 
      #   # This is the local time
      #   # self.velocity = 1.0 / (self._events[-1][TIME] - self._events[-2][TIME])
      # else:
      #   self.velocity = gate / time
      
      # Calculate current velocity based on past course times instead of per-gate distances
      # This assumes that the final average time is the best approximation of the average velocity 
      # given the unknown fixed distance
      self.velocity = \
        (CourseData.AVG_RUNNER_TIMES[gate-1] * CourseData.AVG_RUNNER_TIMES[-1]) / (time * 20)

      self.estimatedTime = (CourseData.NUM_GATES-self.position) / self.velocity + time
      priorRecord = self.courseRecord
      self.courseRecord = ( self.estimatedTime < CourseData.FASTEST_RUNNER_TIMES[-1] )

      if self.courseRecord and not priorRecord:
        logger.info("Runner %d on track for course record with time %fs", self.pId, self.estimatedTime)

    # if(self.id == 1):
      # logger.info("currTime=%fs, runner=%d, position=%d, velocity=%f", time, self._id, self.position, self.velocity)

  def updatePosition(self, currTime):
    if self.position >= CourseData.NUM_GATES:
      return

    # if(self.id == 1):
    #   logger.info("currTime=%fs", currTime)

    # update the velocity and position based on velocity
    self.position = self._events[-1][GATE] + self.velocity*(currTime-self._events[-1][TIME])
    # Make sure that the runner doesn't go through the gate until the passGate event is called
    maxPos = min(self._events[-1][GATE]+0.999, CourseData.NUM_GATES-0.001)
    self.position = min(self.position, maxPos) 

    # dont update the estimated time between gates
    # self.estimatedTime = (CourseData.NUM_GATES-self.position) / self.velocity + currTime
  
  def compare(self, rhs):
    """
    For use in sort(key=lambda p1,p2: return p1.compare(p2))
    """
    if self.estimatedTime > rhs.estimatedTime: return -1
    elif self.estimatedTime < rhs.estimatedTime: return 1
    else: return 0

