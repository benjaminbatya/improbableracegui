import QtQuick 2.15
import QtQuick.Shapes 2.15

Rectangle {
  id: root
 
 Shape {
    anchors.fill: parent

    TrackShape {
      id: trackShape
      strokeWidth: 4
      strokeColor: "red"
    }
  }

  Rectangle {
    id: container

    readonly property int fONT_PIXEL_SIZE : 12

    Connections {
      target: race_model

      function onRunnerAdded(idx) {
        // print("Added %1".arg(idx))

        var comp = runnerElem.createObject(container)
        comp.idx = idx

        var pId = race_model.getPId(idx) 
        comp.displayText = ('00'+pId).slice(-2)

        updateRunner(comp, idx, true)
      }

      function onTimeUpdated(time, doUpdate) {

        for(var ii=0; ii<container.children.length; ++ii) {
          var idx = race_model.getIdx(ii)
          var comp = container.children[idx]
          updateRunner(comp, ii, doUpdate)
        } 
      }

      function updateRunner(comp, ii, doUpdate) {
        var time = race_model.getEstimatedTime(ii)
        time = Math.trunc(time*1000)/1000
        var pos = race_model.getPosition(ii)
        pos = Math.trunc(pos*100)/100

        var percent = pos/race_model.getNumGates();

        var pos = trackShape.pointAtPercent(percent)
        comp.x = pos.x-comp.width/2; 
        comp.y = pos.y-comp.height/2;

        if(percent>=1) {
          comp.x += comp.width*2;
          comp.y = (ii)*comp.height;
          finishLineText.visible = true
        }

        if(doUpdate) {
          comp.color = (ii==0) ? "yellow" : race_model.isOnRecordPace(ii) ? "lightgreen" : race_model.isSlow(ii) ? "red" : "lightsteelblue";
          comp.z = race_model.getNumRunners()-ii
        }
      }
    }

    Component {
      id: runnerElem

      Rectangle {
        color: 'lightsteelblue'
        width: text.implicitWidth + 8
        height: text.implicitHeight
        radius: 8

        border {
          color: "black"
        }

        property int idx : -1
        property string displayText: ""

        Text {
          id: text
          x: 4
          text: displayText
          font.pixelSize: container.fONT_PIXEL_SIZE
        }
      }
    }
  }

  Image {
    id: startingLine

    source: "assets/final/finishingLine.png"
  }

  Component.onCompleted: {
    var pos = trackShape.pointAtPercent(0.0);
    // print("pos=%1".arg(pos))
    startingLine.x = pos.x-startingLine.width/2
    startingLine.y = pos.y-startingLine.height/2

    finishLineText.x = pos.x
    finishLineText.y = -20
  }

  Text {
    id: finishLineText
    text: "Finishing Order"
    visible: false
  }
} 