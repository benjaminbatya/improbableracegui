import QtQuick 2.15
import QtQuick.Shapes 2.15

ShapePath {
  startX: 200; startY: 100
  PathLine { x:200; y:300 }
  PathArc  { x:0; y:300; radiusX:100; radiusY:100 }
  PathLine { x:0; y:100 }
  PathArc  { x:200; y:100; radiusX:100; radiusY:100 }
}