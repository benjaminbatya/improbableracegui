import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 2.15
import QtQuick.Window 2.13

Window {
  readonly property int fONT_PIXEL_SIZE : 12
  readonly property int eLEM_PADDING : fONT_PIXEL_SIZE+8

  width: 900
  height: 1000
  visible: true
  title: qsTr("Race Tracker")

  RowLayout {
    id: controls 
    anchors.top: parent.top

    Text {    
      id: clock
      text: "Clock Time: N/A"
      Connections {
        target: race_model
        function onTimeUpdated(time) {
          clock.text = "Clock Time: %1sec".arg(Math.round(time*100)/100)
        }
      }
    }  
  }
  
  ScrollView {
    id: scrollView
    anchors {
      topMargin: 50
      bottomMargin: 50
      top: controls.bottom
      bottom: parent.bottom
      left: parent.left
    }
    width: parent.width/2
    clip: true

    contentWidth: width
    contentHeight: (dataView.children.length+1)*eLEM_PADDING

    RunnerList {
      id: dataView
      x: 50
    }
  }

  Rectangle {
    id: separator

    anchors {
      topMargin: 50
      top: controls.bottom
      bottom: parent.bottom
      left: scrollView.right
    }
    width: 3

    border {
      color: "black"
      width: 2
    }
  }

  Column {

    anchors {
      topMargin: 50
      // leftMargin: 50
      top: controls.bottom
      bottom: parent.bottom
      left: separator.right
      right: parent.right
    }
    // width: parent.width/2
    
    Text {
      id: label
      anchors.horizontalCenter: parent.horizontalCenter

      text: "Race Track view"
    }
    
    TrackView {
      id: trackView
      anchors {
        top: label.bottom
        left: parent.left
        topMargin: 50
        leftMargin: 100
      }
    
    }
  }

}
