#!/usr/bin/env python3

import socket
import threading
import re
import logging

logger = logging.getLogger("Receiver")

from PySide6.QtCore import QThread, Signal

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 2468  # The default port used by the server

class Receiver(QThread):
  updateReceived = Signal(float, int, int)

  # format is (<elapsed_time> <participant> <gate number>)
  _p = re.compile(r'\(([0-9\.]+)\, ([0-9]+)\, ([0-9]+)\)')
  _done = False

  msg_number = 0

  def setDone(self):
    self._done = True

  def run(self):
      with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        
        s.settimeout(100)
        connected = False
        while not connected:
          if self._done:
            return

          try:
            s.connect((HOST, PORT))
            connected = True
          except (ConnectionRefusedError, ConnectionAbortedError) as _:
            # logger.info("Refused, trying again")
            pass
          
        s.sendall(b'start')

        # s.settimeout(100)

        while not self._done:

          self.msg_number += 1
          # you need to send at least 1 byte to get next message
          s.sendall(bytes(str(self.msg_number), 'utf-8'))
          data = s.recv(1024)
          if (len(data)==0 or data==b'None'):
            logger.info('.......No more data is coming. Exiting.')
            return

          sData = data.decode('utf-8')
          # logger.info(sData)
          m = self._p.match(sData)
          if m==None or m.lastindex!=3:
            logger.info("Invalid parsing. exiting...")
            return

          time = float(m[1])
          pId = int(m[2])
          gate = int(m[3])
          # logger.info("%f, %d, %d", time, pId, gate)
          self.updateReceived.emit(time, pId, gate)
