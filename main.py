#!/usr/bin/env python
# -*- conding: utf-8 -*-

import os
import sys
import urllib.request
import json
import threading
import logging

from Receiver import Receiver
from AnimationModel import AnimationModel

from PySide6.QtQml import QQmlApplicationEngine
from PySide6.QtGui import QGuiApplication

logger = logging.getLogger("main")

if __name__ == "__main__":
  logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

  # Create the various objects
  app = QGuiApplication(sys.argv)
  receiver = Receiver()
  model = AnimationModel(app)

  # Start the Qml engine
  engine = QQmlApplicationEngine()
  engine.rootContext().setContextProperty("race_model", model)
  engine.load(os.path.join(os.path.dirname(__file__), "main.qml"))

  if not engine.rootObjects():
    sys.exit(-1)

  # Connect signals and slots
  receiver.updateReceived.connect(model.onProgress)

  # Start objects up
  receiver.start()
  model.start()

  # execute GUI
  ret = app.exec_()

  logger.info("done...")

  # cleanup
  model.stop()
  receiver.setDone()
  receiver.wait()

  sys.exit(ret)
