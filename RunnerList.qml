import QtQuick 2.15

Rectangle {
  id: runnerList

  readonly property int fONT_PIXEL_SIZE : 12
  readonly property int eLEM_PADDING : fONT_PIXEL_SIZE+8

  Connections {
    target: race_model

    function onRunnerAdded(idx) {
      // print("Added %1".arg(idx))

      var comp = runnerElem.createObject(runnerList)
      comp.idx = idx
      comp.x = 0
      comp.y = 1500
      updateRunner(comp, idx, true)
    }

    function onTimeUpdated(time, doUpdate) {
      for(var ii=0; ii<runnerList.children.length; ++ii) {
        var idx = race_model.getIdx(ii)
        var comp = runnerList.children[idx]
        updateRunner(comp, ii, doUpdate)
      } 
    }

    function updateRunner(comp, ii, doUpdate) {
      var pId = race_model.getPId(ii)
      pId = ('00'+pId).slice(-2)
      var time = race_model.getEstimatedTime(ii)
      time = Math.trunc(time*1000)/1000
      var pos = race_model.getPosition(ii)
      pos = Math.trunc(pos*100)/100
      var velo = race_model.getVelocity(ii)
      velo= Math.trunc(velo*100)/100

      var timeStr = (pos>=race_model.getNumGates()) ? '<img src="assets/final/completed.png">%1hr'.arg(time) : '<img src="assets/final/running.png">%1hr'.arg(time)

      comp.displayText = "<b>Runner:</b>%1 %2 <b>Pos:</b>%3 <b>Velocity:</b>%4".
        arg(pId).arg(timeStr).arg(pos).arg(velo)

      if(doUpdate) {

        comp.nextY = ii*(runnerList.eLEM_PADDING)
        if(comp.y < comp.nextY) { 
          // print("moving %1 down".arg(pId))
          comp.nextX = -25; 
        } else if(comp.y > comp.nextY) { 
          // print("moving %1 up".arg(pId))
          comp.nextX = 25; 
        }
        else { comp.nextX = 0; }

        comp.z = race_model.getNumRunners()-ii;
        comp.color = (ii==0) ? "yellow" : race_model.isOnRecordPace(ii) ? "lightgreen" : race_model.isSlow(ii) ? "red" : "lightsteelblue";
      }
    }
  }

  Component {
    id: runnerElem

    Rectangle {
      color: 'lightsteelblue'
      width: text.implicitWidth + 8
      height: text.implicitHeight
      radius: 4

      border {
        color: "black"
      }

      property int idx : -1
      property string displayText: ""
      property int nextX : 0;
      property int nextY : -10;

      Text {
        id: text
        x: 4
        text: displayText
        font.pixelSize: runnerList.fONT_PIXEL_SIZE
      }

      BounceBehavior on nextY {}
    }
  }

}