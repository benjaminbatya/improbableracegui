NUM_GATES = 20

AVG_RUNNER_TIMES = [ 1.48,  3.38,  5.39,  7.43,  9.17, 10.85, 12.18, 13.23, 14.02, 14.62,  \
15.14, 15.64, 16.19, 16.84, 17.63, 18.49, 19.33, 20.17, 20.82, 21.18]

FASTEST_RUNNER_TIMES = [ 0.74,  1.41,  1.68,  2.57,  4.43,  5.28,  6.24,  6.76,  7.48,  7.55, \
7.82,  8.21,  8.61,  9.24,  9.56, 10.4,  11.1,  12.0,   12.23, 12.56]